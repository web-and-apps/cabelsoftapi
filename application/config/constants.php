<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

//////////////////HTTP STATUS
define('HTTP_200', 200);
define('HTTP_201', 201);
define('HTTP_400', 400);
define('HTTP_401', 401);
//////////////////HTTP STATUS

////common
define('NEED_ALL_PARAMS', 'Please give all request params');
define('SOMETHING_WRONG_ADD_DATA', 'Something Wrong, while Add Data');
define('SOMETHING_WRONG_UPDATE_DATA', 'Something Wrong, while Update Data');
define('SOMETHING_WRONG_DELETE_DATA', 'Something Wrong, while Delete Data');
define('SOMETHING_WRONG_RECEIVING_DATA', 'Something Wrong, while Receiving Data');
define('PAGE_COUNT_MISSING', 'Page Count must be not empty');
define('USER_ID_MISSING', 'User Id Missing. Unable to process Request');
///common

///expense constants
define('NEED_EXPENSE_AMOUNT', 'Enter Expense Amount');
define('NEED_EXPENSE_COMMENTS', 'Enter Expense Comment');
define('NEED_EXPENSE_TYPE', 'Enter Expense Type');
define('EXPENSE_ENTRY_ADDED', 'New Expense Added Successfully');
define('EXPENSE_ENTRY_UPDATED', 'Expense Updated Successfully');
define('EXPENSE_ENTRY_DELETED', 'Expense Deleted Successfully');
define('EXPENSE_ID_MISSING', 'Expense Id Missing. Unable to process Request');
define('EXPENSE_LIST_FOUND', 'Expense List generated Successfully');
define('EXPENSE_LIST_EMPTY', 'Expense List Empty');
///expense constants

////plan constants
define('NEED_PLAN_NAME', 'Enter Plan Name');
define('NEED_PLAN_COST', 'Enter Plan Cost');
define('NEED_PLAN_DESCRIPTION', 'Enter Plan Description');
define('PLAN_ENTRY_ADDED', 'New Plan Added Successfully');
define('PLAN_ID_MISSING', 'Plan Id Missing. Unable to process Request');
define('PLAN_ENTRY_UPDATED', 'Plan Updated Successfully');
define('PLAN_ENTRY_DELETED', 'Plan Deleted Successfully');
define('PLAN_LIST_FOUND', 'Plan List generated Successfully');
define('PLAN_LIST_EMPTY', 'Plan List Empty');
////plan constants


//customer profile a data
define('NEED_CUSTOMER_NAME', 'Enter Customer Name');
define('NEED_CUSTOMER_AREA', 'Enter Customer Area');
define('PROFILE_A_DETAILS_RECEIVED', 'Profile A Details Received Successfully');
define('NEED_CUSTOMER_STREET', 'Enter Customer Street');
define('NEED_CUSTOMER_HOME_TYPE', 'Enter Customer Home Type');
define('NEED_CUSTOMER_MOBILE', 'Enter Customer Mobile Number');
define('CUSTOMER_ENTRY_ADDED', 'New Customer Added Successfully');
define('CUSTOMER_ID_MISSING', 'Customer Id Missing. Unable to process Request');
define('CUSTOMER_ENTRY_UPDATED', 'Customer Updated Successfully');
define('CUSTOMER_ENTRY_DELETED', 'Customer Deleted Successfully');
define('CUSTOMER_LIST_FOUND', 'Customer List generated Successfully');
define('CUSTOMER_LIST_EMPTY', 'Customer List Empty');
define('OPERATION_ADDED', 'ADDED');
define('OPERATION_UPDATED', 'UPDATED');
//customer profile a data


//customer profile b data
define('NEED_CUSTOMER_BILLING_NAME', 'Enter Customer Billing Name');
define('NEED_CUSTOMER_BILLING_EMAIL', 'Enter Customer Email');
define('NEED_CUSTOMER_AADHAR_NO', 'Enter Customer Aadhar no');
define('NEED_CUSTOMER_EB_NO', 'Enter Customer EB No');
define('NEED_CUSTOMER_PAN_NO', 'Enter Customer PAN no');
define('NEED_CUSTOMER_SECURITY_DEPOSITE', 'Enter Security Deposite');
define('CUSTOMER_B_ENTRY_ADDED', 'Customer Profile B Added Successfully');
define('CUSTOMER_B_ENTRY_UPDATE', 'Customer Profile B Updated Successfully');
define('PROFILE_B_DETAILS_RECEIVED', 'Profile B Details Received Successfully');
//customer profile b data


//customer profile c data
define('NEED_CUSTOMER_BILLING_TYPE', 'Enter Customer Billing Type');
define('NEED_CUSTOMER_BILLING_TIME', 'Enter Customer Time');
define('NEED_CUSTOMER_ADDITIONAL_CHARGE', 'Enter Customer Additional Charge');
define('CUSTOMER_C_ENTRY_ADDED', 'Customer Profile C Added Successfully');
define('CUSTOMER_C_ENTRY_UPDATE', 'Customer Profile C Updated Successfully');
define('PROFILE_C_DETAILS_RECEIVED', 'Profile C Details Received Successfully');
//customer profile c data

//customer profile d data
define('NEED_CUSTOMER_SETTOP_NO', 'Enter Settop Box no');
define('NEED_CUSTOMER_SETTOP_NAME', 'Enter Settop Box name');
define('NEED_CUSTOMER_SETTOP_TYPE', 'Enter Settop Box type');
define('NEED_CUSTOMER_SMART_NO', 'Enter Smart Card no');
define('EMPTY_SETTOP_BOX', 'Settop Box List Empty');
define('CUSTOMER_D_ENTRY_ADDED', 'Customer Profile D Added Successfully');
define('CUSTOMER_D_ENTRY_UPDATE', 'Customer Profile D Updated Successfully');
define('PROFILE_D_DETAILS_RECEIVED', 'Profile D Details Received Successfully');
//customer profile d data


//customer profile e data
define('NEED_CUSTOMER_CONNECTION_DATE', 'Enter Connection date');
define('NEED_CUSTOMER_REGISTER_DATE', 'Enter Register date');
define('CUSTOMER_E_ENTRY_ADDED', 'Customer Profile E Added Successfully');
define('CUSTOMER_E_ENTRY_UPDATE', 'Customer Profile E Updated Successfully');
define('PROFILE_E_DETAILS_RECEIVED', 'Profile E Details Received Successfully');
define('CUSTOMER_STATUS_YES', 'Y');
define('CUSTOMER_STATUS_PROGRESS', 'P');
define('CUSTOMER_STATUS_NO', 'N');
define('CUSTOMER_PROFILE_STATUS', 'Customer Profile Status  Received Successfully');
//customer profile e data

///profile data
define('NOMINEE_RELATION_SUCCESS', 'Nominee Relations Details Received Successfully');
define('NOMINEE_RELATION_NOT_FOUND', 'Nominee Relations Not Found');
define('USER_DETAILS_UPDATED', 'User Details Updated Successfully');
define('BANK_DETAILS_UPDATED', 'Bank Details Updated Successfully');
define('NOMINEE_DETAILS_UPDATED', 'Nominee Details Updated Successfully');
define('PROFILE_IMAGE_UPDATED', 'Profile Image Uploaded Successfully');
define('BANNER_IMAGE_UPDATED', 'Banner Image Uploaded Successfully');
define('PAN_IMAGE_UPDATED', 'PAN Image Uploaded Successfully');
define('ADDRESS_IMAGE_UPDATED', 'Address proof Image Uploaded Successfully');
define('IMAGE_TAG_MISSING', 'Image TAG Missing');
define('IMAGE_MISSING', 'Upload Image First');
define('BANNER_TAG', 'BANNER');
define('PROFILE_TAG', 'PROFILE');
define('PAN_TAG', 'PAN');
define('ADDRESS_TAG', 'ADDRESS');
///profile data





