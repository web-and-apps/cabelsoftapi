<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class PlanModel extends CI_Model{

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}


	public function getPlanDetails($data){
		$this->db->select('*');
		$this->db->from('plan_master');
		$this->db->where($data);
		$query_result=$this->db->get();
		return $query_result->result_array();
	} 

	public function getPlanDetailsPage($pagecount,$searchhint){
		$this->db->select('*');
		$this->db->from('plan_master');
		$this->db->group_start();
		$this->db->like('plan_name',$searchhint, 'after');
		$this->db->or_like('plan_cost', $searchhint);
		$this->db->or_like('plan_description', $searchhint);
		$this->db->group_end();
		$this->db->limit(10,$pagecount); 
		$query_result=$this->db->get();
		return $query_result->result_array();
	} 

	public function getPlanDetailsDate($from_date,$to_date,$pagecount,$searchhint){
		$this->db->select('*');
		$this->db->from('plan_master');
		$this->db->group_start();
		$this->db->like('plan_name',$searchhint, 'after');
		$this->db->or_like('plan_cost', $searchhint);
		$this->db->or_like('plan_description', $searchhint);
		$this->db->group_end();
		$this->db->where('plan_master.plan_date >=', $from_date);
		$this->db->where('plan_master.plan_date <=', $to_date);
		$this->db->limit(10,$pagecount); 

		$query_result=$this->db->get();
		return $query_result->result_array();
	} 



	public function deletePlanModel($id){
		$this->db
		->where('plan_id', $id)
		->delete('plan_master');
		if ($this->db->affected_rows() >= 0) {
			return true;
		}else{
			return false;
		}
	}


	public function updatePlanDatas($id,$data){
		$this->db
		->where('plan_id', $id)
		->update('plan_master', $data);
		if ($this->db->affected_rows() >= 0) {
			return true;
		}else{
			return false;
		}
	}




	public function addPlanModel($data){
		$insert = $this->db->insert('plan_master',$data);
		if($insert){
			return $this->db->insert_id();
		}
		else{
			return false;
		}
	}




}?>