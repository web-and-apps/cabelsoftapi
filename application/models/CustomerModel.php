<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class CustomerModel extends CI_Model{

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}


	public function getCustomerDetails($data){
		$this->db->select('*');
		$this->db->from('customer_master');
		$this->db->where($data);
		$query_result=$this->db->get();
		return $query_result->result_array();
	} 

	public function getCustomerDetailsPage($userid,$pagecount,$searchhint){
		$this->db->select('*');
		$this->db->from('customer_master');
		$this->db->group_start();
		$this->db->like('customer_name',$searchhint, 'after');
		$this->db->or_like('customer_mobile', $searchhint);
		$this->db->group_end();
		$this->db->where('user_id', $userid);
		$this->db->limit(10,$pagecount); 
		$query_result=$this->db->get();
		return $query_result->result_array();
	} 

	public function getCustomerDetailsDate($userid,$from_date,$to_date,$pagecount,$searchhint){
		$this->db->select('*');
		$this->db->from('customer_master');
		$this->db->group_start();
		$this->db->like('customer_name',$searchhint, 'after');
		$this->db->or_like('customer_mobile', $searchhint);
		$this->db->group_end();
		$this->db->where('customer_master.customer_date >=', $from_date);
		$this->db->where('customer_master.customer_date <=', $to_date);
		$this->db->where('user_id', $userid);
		$this->db->limit(10,$pagecount); 
		$query_result=$this->db->get();
		return $query_result->result_array();
	} 



	public function deleteCustomerModel($id){
		$tables = array('customer_master', 'customer_profile_b', 'customer_profile_c','customer_profile_d','customer_profile_e','customer_profile_status');
		$this->db->where('customer_id', $id);
		$this->db->delete($tables);
		if ($this->db->affected_rows() >= 0) {
			return true;
		}else{
			return false;
		}
	}


////////////////////////// A


	public function getProfileADetails($customer_id){
		$this->db->select('*');
		$this->db->from('customer_master');
		$this->db->where('customer_id',$customer_id);
		//$this->db->where($data);
		$query_result=$this->db->get();
		return $query_result->result_array();
	} 


	public function updateCustomerDatas($id,$data){
		$this->db
		->where('customer_id', $id)
		->update('customer_master', $data);
		if ($this->db->affected_rows() >= 0) {
			return true;
		}else{
			return false;
		}
	}




	public function addCustomerModel($data){
		$insert = $this->db->insert('customer_master',$data);
		if($insert){
			return $this->db->insert_id();
		}
		else{
			return false;
		}
	}

	/////////////////////////// A


	/////////////////////////// B


	public function getProfileBDetails($customer_id){
		$this->db->select('*');
		$this->db->from('customer_profile_b');
		$this->db->where('customer_id',$customer_id);
		//$this->db->where($data);
		$query_result=$this->db->get();
		return $query_result->result_array();
	} 


	public function updateCustomerBDatas($id,$data){
		$this->db
		->where('customer_b_id', $id)
		->update('customer_profile_b', $data);
		if ($this->db->affected_rows() >= 0) {
			return true;
		}else{
			return false;
		}
	}




	public function addCustomerBModel($data){
		$insert = $this->db->insert('customer_profile_b',$data);
		if($insert){
			return $this->db->insert_id();
		}
		else{
			return false;
		}
	}

	//////////////////////////B


		/////////////////////////// C


	public function getProfileCDetails($customer_id){
		$this->db->select('*');
		$this->db->from('customer_profile_c');
		$this->db->where('customer_id',$customer_id);
		//$this->db->where($data);
		$query_result=$this->db->get();
		return $query_result->result_array();
	} 
	

	public function updateCustomerCDatas($id,$data){
		$this->db
		->where('customer_c_id', $id)
		->update('customer_profile_c', $data);
		if ($this->db->affected_rows() >= 0) {
			return true;
		}else{
			return false;
		}
	}




	public function addCustomerCModel($data){
		$insert = $this->db->insert('customer_profile_c',$data);
		if($insert){
			return $this->db->insert_id();
		}
		else{
			return false;
		}
	}

	//////////////////////////C

	/////////////////////////// D


	public function getProfileDDetails($customer_id){
		$this->db->select('*');
		$this->db->from('customer_profile_d');
		$this->db->where('customer_id',$customer_id);
		//$this->db->where($data);
		$query_result=$this->db->get();
		return $query_result->result_array();
	} 


	public function getAllSettopBoxDatas($customer_id){
		$this->db->select('*');
		$this->db->from('customer_profile_d');
		$this->db->where('customer_id',$customer_id);
		$query_result=$this->db->get();
		return $query_result->result_array();
	} 
	

	public function updateCustomerDDatas($id,$data){
		$this->db
		->where('customer_d_id', $id)
		->update('customer_profile_d', $data);
		if ($this->db->affected_rows() >= 0) {
			return true;
		}else{
			return false;
		}
	}




	public function addCustomerDModel($data){
		$insert = $this->db->insert('customer_profile_d',$data);
		if($insert){
			return $this->db->insert_id();
		}
		else{
			return false;
		}
	}

	////////////////////////// D

		/////////////////////////// E


	public function getCustomerEDetails($customer_id){
		$this->db->select('*');
		$this->db->from('customer_profile_e');
		$this->db->where('customer_id',$customer_id);
		//$this->db->where($data);
		$query_result=$this->db->get();
		return $query_result->result_array();
	} 
	

	public function updateCustomerEDatas($id,$data){
		$this->db
		->where('customer_e_id', $id)
		->update('customer_profile_e', $data);
		if ($this->db->affected_rows() >= 0) {
			return true;
		}else{
			return false;
		}
	}




	public function addCustomerEModel($data){
		$insert = $this->db->insert('customer_profile_e',$data);
		if($insert){
			return $this->db->insert_id();
		}
		else{
			return false;
		}
	}

	//////////////////////////E

	//////customerStatus
	public function addCustomerStatusModel($data){
		$insert = $this->db->insert('customer_profile_status',$data);
		if($insert){
			return $this->db->insert_id();
		}
		else{
			return false;
		}
	}


	public function updateCustomerStatusDatas($id,$data){
		$this->db
		->where('customer_id', $id)
		->update('customer_profile_status', $data);
		if ($this->db->affected_rows() >= 0) {
			return true;
		}else{
			return false;
		}
	}

	public function getCustomerStatusDetails($customer_id){
		$this->db->select('*');
		$this->db->from('customer_profile_status');
		$this->db->where('customer_id',$customer_id);
		//$this->db->where($data);
		$query_result=$this->db->get();
		return $query_result->result_array();
	} 

    //////customerStatus


}?>