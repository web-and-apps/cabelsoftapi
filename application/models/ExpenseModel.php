<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class ExpenseModel extends CI_Model{

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}


	public function getExpenseDetails($data){
		$this->db->select('*');
		$this->db->from('expense_master');
		$this->db->where($data);
		$query_result=$this->db->get();
		return $query_result->result_array();
	} 

	public function getExpenseDetailsPage($data,$pagecount,$searchhint){
		$this->db->select('*');
		$this->db->from('expense_master');
		$this->db->group_start();
		$this->db->like('expense_comment',$searchhint, 'after');
		$this->db->or_like('expense_amount', $searchhint);
		$this->db->group_end();
		$this->db->where('expense_master.user_id',$data);
		$this->db->limit(25,$pagecount); 
		$query_result=$this->db->get();
		return $query_result->result_array();
	} 

	public function getExpenseDetailsDate($data,$from_date,$to_date,$pagecount,$searchhint){
		$this->db->select('*');
		$this->db->from('expense_master');
		$this->db->group_start();
		$this->db->like('expense_comment',$searchhint, 'after');
		$this->db->or_like('expense_amount', $searchhint);
		$this->db->group_end();
		$this->db->where('expense_master.expense_date >=', $from_date);
		$this->db->where('expense_master.expense_date <=', $to_date);
		$this->db->where('expense_master.user_id',$data);
		$this->db->limit(25,$pagecount); 
		$query_result=$this->db->get();
		return $query_result->result_array();
	} 



	public function deleteExpenseModel($id){
		$this->db
		->where('expense_id', $id)
		->delete('expense_master');
		if ($this->db->affected_rows() >= 0) {
			return true;
		}else{
			return false;
		}
	}


	public function updateExpenseDatas($id,$data){
		$this->db
		->where('expense_id', $id)
		->update('expense_master', $data);
		if ($this->db->affected_rows() >= 0) {
			return true;
		}else{
			return false;
		}
	}


	public function addExpenseModel($data){
		$insert = $this->db->insert('expense_master',$data);
		if($insert){
			return $this->db->insert_id();
		}
		else{
			return false;
		}
	}




}?>