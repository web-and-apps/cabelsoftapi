<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class ProfileModel extends CI_Model{

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}


	public function getUserDetails($data){
		$this->db->select('*');
		$this->db->from('nominee_master');
		$this->db->join('user_registration','nominee_master.user_id=user_registration.user_id');
		$this->db->join('bank_master','bank_master.user_id=user_registration.user_id');
		$this->db->where('user_registration.user_id',$data);
		//$this->db->where($data);
		$query_result=$this->db->get();
		return $query_result->result_array();
	} 


	public function updateUserDatas($id,$data){
		$this->db
		->where('user_id',$id)
		->update('user_registration',$data);
		if ($this->db->affected_rows() >= 0) {
			return true;
		}else{
			return false;
		}
	}

	public function updateBankImageData($id,$data){
		$this->db
		->where('user_id',$id)
		->update('bank_master',$data);
		if ($this->db->affected_rows() >= 0) {
			return true;
		}else{
			return false;
		}
	}


	public function getNomineeDetails($data){
		$this->db->select('*');
		$this->db->from('nominee_master');
		$this->db->where('nominee_master.user_id',$data);
		//$this->db->where($data);
		$query_result=$this->db->get();
		return $query_result->result_array();
	} 


	public function updateNomineeDatas($id,$data){
		$this->db
		->where('user_id',$id)
		->update('nominee_master',$data);
		if ($this->db->affected_rows() >= 0) {
			return true;
		}else{
			return false;
		}
	}

	public function getBankDetails($data){
		$this->db->select('*');
		$this->db->from('bank_master');
		$this->db->where('bank_master.user_id',$data);
		//$this->db->where($data);
		$query_result=$this->db->get();
		return $query_result->result_array();
	} 

	public function getNomineeRelation(){
		$this->db->select('*');
		$this->db->from('nominee_relation_master');
		$query_result=$this->db->get();
		return $query_result->result_array();
	} 


	public function updateBankDatas($id,$data){
		$this->db
		->where('user_id',$id)
		->update('bank_master',$data);
		if ($this->db->affected_rows() >= 0) {
			return true;
		}else{
			return false;
		}
	}


}?>