<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'libraries/API_Controller.php');

class PlanController extends API_Controller{


	public function __construct()
	{
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();


    $this->_APIConfig([
      'methods'                              => ['POST','GET'],
      'requireAuthorization'                 => true,
      'limit' => [100, 'ip', 'everyday'] ,
      'data' => [ 'status_code' => HTTP_401 ],
    ]);

    $this->load->model('PlanModel');
  }



  public function addNewPlanDatas(){

   $json_request_body = file_get_contents('php://input');
   $data = json_decode($json_request_body, true);

   if(isset($data['plan_name']) && isset($data['plan_cost']) && isset($data['plan_description'])){

     $plan_name = $data['plan_name'];
     $plan_cost = $data['plan_cost'];    
     $plan_description = $data['plan_description']; 


     if(empty($plan_name)){
      $response_array = array(
       'status_code' => HTTP_400,
       'message' => NEED_PLAN_NAME,
     );
      $this->output
      ->set_content_type('application/json')
      ->set_output(json_encode($response_array));
    }
    else if(empty($plan_cost)){
      $response_array = array(
       'status_code' => HTTP_400,
       'message' => NEED_PLAN_COST,
     );
      $this->output
      ->set_content_type('application/json')
      ->set_output(json_encode($response_array));
    }
    else if(empty($plan_description)){
      $response_array = array(
       'status_code' => HTTP_400,
       'message' => NEED_PLAN_DESCRIPTION,
     );
      $this->output
      ->set_content_type('application/json')
      ->set_output(json_encode($response_array));
    }
    else{
      $plan_array = array(
        'plan_name' => $plan_name,
        'plan_cost' => $plan_cost,
        'plan_description' => $plan_description
      );

      $result_query = $this->PlanModel->addPlanModel($plan_array);
      if($result_query)
      {
        $response_array = array(
         'status_code' => HTTP_200,
         'message' => PLAN_ENTRY_ADDED
       );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }
      else{
        $response_array = array(
         'status_code' => HTTP_400,
         'message' => SOMETHING_WRONG_ADD_DATA,
       );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }

    }
  }
  else{
    $response_array = array(
      'status_code' => HTTP_400,
      'message' => NEED_ALL_PARAMS,
    );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }
}

public function updatePlan(){
 $json_request_body = file_get_contents('php://input');
 $data = json_decode($json_request_body, true);


 if(isset($data['plan_id']) && isset($data['plan_name']) && isset($data['plan_cost']) && isset($data['plan_description'])){

   $plan_id = $data['plan_id'];
   $plan_name = $data['plan_name'];
   $plan_cost = $data['plan_cost'];
   $plan_description = $data['plan_description'];

   if(empty($plan_id)){
    $response_array = array(
     'status_code' => HTTP_400,
     'message' => PLAN_ID_MISSING,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }else{
    $plan_array = array('plan_id' => $plan_id);
    $result_query = $this->PlanModel->getPlanDetails($plan_array);
    $db_plan_name = $result_query[0]['plan_name'];
    $db_plan_cost = $result_query[0]['plan_cost'];
    $db_plan_description = $result_query[0]['plan_description'];

    if(empty($plan_name)){
      $plan_name = $db_plan_name;
    } if(empty($plan_cost)){
      $plan_cost=$db_plan_cost;
    }if(empty($plan_description)){
      $plan_description=$db_plan_description;
    }


    $plan_data = array(
      'plan_name' => $plan_name,
      'plan_cost' => $plan_cost,
      'plan_description' => $plan_description
    );
    $result_query = $this->PlanModel->updatePlanDatas($plan_id,$plan_data);
    if($result_query)
    {
      $response_array = array(
        'status_code' => HTTP_200,
        'message' => PLAN_ENTRY_UPDATED,
      );
      $this->output
      ->set_content_type('application/json')
      ->set_output(json_encode($response_array));
    }
    else{
      $response_array = array(
        'status_code' =>HTTP_400,
        'message' => SOMETHING_WRONG_UPDATE_DATA,
      );
      $this->output
      ->set_content_type('application/json')
      ->set_output(json_encode($response_array));
    }
  }
}else{
  $response_array = array(
    'status_code' => HTTP_400,
    'message' => NEED_ALL_PARAMS,
  );
  $this->output
  ->set_content_type('application/json')
  ->set_output(json_encode($response_array));
}

}



public function deletePlan(){
  $json_request_body = file_get_contents('php://input');
  $data = json_decode($json_request_body, true);

  if(isset($data['plan_id'])){
    $plan_id=$data['plan_id'];

    if(empty($plan_id)){
      $response_array = array(
        'status_code' => HTTP_400,
        'message' => PLAN_ID_MISSING,
      );
      $this->output
      ->set_content_type('application/json')
      ->set_output(json_encode($response_array));
    }
    else{
      $result_query = $this->PlanModel->deletePlanModel($plan_id);
      if($result_query)
      {
        $response_array = array(
          'status_code' => HTTP_200,
          'message' => PLAN_ENTRY_DELETED
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }
      else{
        $response_array = array(
          'status_code' => HTTP_400,
          'message' => SOMETHING_WRONG_DELETE_DATA
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }
    }
  }
  else{
    $response_array = array(
      'status_code' => HTTP_400,
      'message' => NEED_ALL_PARAMS
    );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }
}



public function getAllPlanDetails(){
  $json_request_body = file_get_contents('php://input');
  $data = json_decode($json_request_body, true);

  if(isset($data['from_date']) && isset($data['to_date']) && isset($data['page_count']) && isset($data['search_keyword'])){

    $from_date = $data['from_date'];
    $to_date = $data['to_date'];
    $page_count = $data['page_count'];
    $search_keyword = $data['search_keyword'];

    if($page_count==''){
      $response_array = array(
        'status_code' => HTTP_400,
        'message' => PAGE_COUNT_MISSING,
      );
      $this->output
      ->set_content_type('application/json')
      ->set_output(json_encode($response_array));
    }else{
      $page_count = ($page_count * 10);
      if(empty($from_date) && empty($to_date)){
        $result_query = $this->PlanModel->getPlanDetailsPage($page_count,$search_keyword);
      }else{
        $result_query = $this->PlanModel->getPlanDetailsDate($from_date,$to_date,$page_count,$search_keyword);
      }

      $resultSet = Array();
      if($result_query)
      {
        foreach ($result_query as $expense_result) 
        { 
          $resultSet[] = array(
            "plan_id" =>  $expense_result['plan_id'],
            "plan_name" =>  $expense_result['plan_name'],
            "plan_cost" =>  $expense_result['plan_cost'],
            "plan_description" =>  $expense_result['plan_description'],
            "plan_date" =>  $expense_result['plan_date']
          );
        } 

        $response_array = array(
          'status_code' => HTTP_200,
          'message' => PLAN_LIST_FOUND,
          'product_details' => $resultSet
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }
      else{
        $response_array = array(
          'status_code' => HTTP_400,
          'message' => PLAN_LIST_EMPTY,
          'product_details' => $resultSet
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }
    }



  }
  else{
    $response_array = array(
      'status_code' => HTTP_400,
      'message' => NEED_ALL_PARAMS
    );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }

}



}



?>
