<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'libraries/API_Controller.php');

class CustomerBController extends API_Controller{


	public function __construct()
	{
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();


    $this->_APIConfig([
      'methods'                              => ['POST','GET'],
      'requireAuthorization'                 => true,
      'limit' => [100, 'ip', 'everyday'] ,
      'data' => [ 'status_code' => HTTP_401 ],
    ]);

    $this->load->model('CustomerModel');
  }



  public function profileBCustomerDatas(){

   $json_request_body = file_get_contents('php://input');
   $data = json_decode($json_request_body, true);

   if(isset($data['user_id']) &&
    isset($data['customer_id']) &&
    isset($data['customer_b_billing_name']) && 
    isset($data['customer_b_email']) && 
    isset($data['customer_b_aadhar_no']) &&
    isset($data['customer_b_eb_no']) &&
    isset($data['customer_b_pan_no']) && 
    isset($data['customer_b_security_deposite'])){


     $user_id = $data['user_id'];
   $customer_id = $data['customer_id'];
   $customer_b_billing_name = $data['customer_b_billing_name'];
   $customer_b_email = $data['customer_b_email'];
   $customer_b_aadhar_no = $data['customer_b_aadhar_no'];
   $customer_b_eb_no = $data['customer_b_eb_no'];
   $customer_b_pan_no = $data['customer_b_pan_no'];  
   $customer_b_security_deposite = $data['customer_b_security_deposite'];  
   $customer_b_id = $data['customer_b_id'];  

   if(empty($user_id)){
    $response_array = array(
     'status_code' => HTTP_400,
     'operation' => "",
     'message' => USER_ID_MISSING,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }   
  else if(empty($customer_id)){
    $response_array = array(
     'status_code' => HTTP_400,
     'operation' => "",
     'message' => CUSTOMER_ID_MISSING,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }
  else if(empty($customer_b_billing_name)){
    $response_array = array(
     'status_code' => HTTP_400,
     'operation' => "",
     'message' => NEED_CUSTOMER_BILLING_NAME,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }
  else if(empty($customer_b_email)){
    $response_array = array(
     'status_code' => HTTP_400,
     'operation' => "",
     'message' => NEED_CUSTOMER_BILLING_EMAIL,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }
  else if(empty($customer_b_aadhar_no)){
    $response_array = array(
     'status_code' => HTTP_400,
     'operation' => "",
     'message' => NEED_CUSTOMER_AADHAR_NO,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }
  else if(empty($customer_b_eb_no)){
    $response_array = array(
     'status_code' => HTTP_400,
     'operation' => "",
     'message' => NEED_CUSTOMER_EB_NO,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }
  else if(empty($customer_b_pan_no)){
    $response_array = array(
     'status_code' => HTTP_400,
     'operation' => "",
     'message' => NEED_CUSTOMER_PAN_NO,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }
  else if(empty($customer_b_security_deposite)){
    $response_array = array(
     'status_code' => HTTP_400,
     'operation' => "",
     'message' => NEED_CUSTOMER_SECURITY_DEPOSITE,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }
  else{

    if(empty($data['customer_b_id'])){

      $customer_array = array(
        'user_id' => $user_id,
        'customer_id' => $customer_id,
        'customer_b_billing_name' => $customer_b_billing_name,
        'customer_b_email' => $customer_b_email,
        'customer_b_aadhar_no' => $customer_b_aadhar_no,
        'customer_b_eb_no' => $customer_b_eb_no,
        'customer_b_pan_no' => $customer_b_pan_no,
        'customer_b_security_deposite' => $customer_b_security_deposite
      );

      $result_query = $this->CustomerModel->addCustomerBModel($customer_array);
      if($result_query)
      {

        $customer_status_array = array(
         'customer_b_status' => CUSTOMER_STATUS_YES,
         'customer_c_status' => CUSTOMER_STATUS_PROGRESS,
       );
        $this->CustomerModel->updateCustomerStatusDatas($customer_id,$customer_status_array);
        $response_array = array(
         'status_code' => HTTP_200,
         'operation' => OPERATION_ADDED,
         'message' => CUSTOMER_B_ENTRY_ADDED
       );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }
      else{
        $response_array = array(
         'status_code' => HTTP_400,
         'operation' => "",
         'message' => SOMETHING_WRONG_ADD_DATA,
       );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }

    }else{
      $customer_array = array(
        'customer_b_billing_name' => $customer_b_billing_name,
        'customer_b_email' => $customer_b_email,
        'customer_b_aadhar_no' => $customer_b_aadhar_no,
        'customer_b_eb_no' => $customer_b_eb_no,
        'customer_b_pan_no' => $customer_b_pan_no,
        'customer_b_security_deposite' => $customer_b_security_deposite
      );


      $result_query = $this->CustomerModel->updateCustomerBDatas($customer_b_id,$customer_array);
      if($result_query)
      {
        $response_array = array(
         'status_code' => HTTP_200,
         'operation' => OPERATION_UPDATED,
         'message' => CUSTOMER_B_ENTRY_UPDATE
       );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }
      else{
        $response_array = array(
         'status_code' => HTTP_400,
         'operation' => "",
         'message' => SOMETHING_WRONG_UPDATE_DATA,
       );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }
    }

  }
}
else{
  $response_array = array(
    'status_code' => HTTP_400,
    'operation' => "",
    'message' => NEED_ALL_PARAMS,
  );
  $this->output
  ->set_content_type('application/json')
  ->set_output(json_encode($response_array));
}
}


public function getprofileBCustomerDetails(){
  header("Access-Control-Allow-Origin: *");

  $this->load->model('ProfileModel');
  $json_request_body = file_get_contents('php://input');
  $data = json_decode($json_request_body, true);

  if(isset($data['customer_id'])){
    $customer_id = $data['customer_id'];

    if(empty($customer_id)){
      $response_array = array(
       'status_code' => HTTP_201,
       'message' => CUSTOMER_ID_MISSING,
       'profile_b_details' => array('customer_b_id' => "",
        'user_id' => "",
        'customer_id' => "",
        'customer_b_billing_name' => "",
        'customer_b_email' => "",
        'customer_b_aadhar_no' => "",
        'customer_b_eb_no' => "",
        'customer_b_pan_no' => "",
        'customer_b_security_deposite' => "",
        'customer_b_date' => "",
      )
     );
      $this->output
      ->set_content_type('application/json')
      ->set_status_header(HTTP_201)
      ->set_output(json_encode($response_array));
    }else{
      $result_query = $this->CustomerModel->getProfileBDetails($customer_id);
      if($result_query)
      {
        $response_array = array(
          'status_code' => HTTP_200,
          'message' => PROFILE_B_DETAILS_RECEIVED,
          'profile_b_details' => array('customer_b_id' => $result_query[0]['customer_b_id'],
            'user_id' => $result_query[0]['user_id'],
            'customer_id' => $result_query[0]['customer_id'],
            'customer_b_billing_name' => $result_query[0]['customer_b_billing_name'],
            'customer_b_email' => $result_query[0]['customer_b_email'],
            'customer_b_aadhar_no' => $result_query[0]['customer_b_aadhar_no'],
            'customer_b_eb_no' => $result_query[0]['customer_b_eb_no'],
            'customer_b_pan_no' => $result_query[0]['customer_b_pan_no'],
            'customer_b_security_deposite' => $result_query[0]['customer_b_security_deposite'],
            'customer_b_date' => $result_query[0]['customer_b_date'],
          )
        );
        $this->output
        ->set_content_type('application/json')
        ->set_status_header(HTTP_200)
        ->set_output(json_encode($response_array));
      }
      else{
        $response_array = array(
          'status_code' => HTTP_201,
          'message' => SOMETHING_WRONG_RECEIVING_DATA,
          'profile_b_details' => array('customer_b_id' => "",
            'user_id' => "",
            'customer_id' => "",
            'customer_b_billing_name' => "",
            'customer_b_email' => "",
            'customer_b_aadhar_no' => "",
            'customer_b_eb_no' => "",
            'customer_b_pan_no' => "",
            'customer_b_security_deposite' => "",
            'customer_b_date' => "",
          )
        );
        $this->output
        ->set_content_type('application/json')
        ->set_status_header(HTTP_201)
        ->set_output(json_encode($response_array));
      }

    }
  }
  else{
    $response_array = array(
      'status_code' => HTTP_201,
      'message' => NEED_ALL_PARAMS,
      'profile_b_details' => array('customer_b_id' => "",
        'user_id' => "",
        'customer_id' => "",
        'customer_b_billing_name' => "",
        'customer_b_email' => "",
        'customer_b_aadhar_no' => "",
        'customer_b_eb_no' => "",
        'customer_b_pan_no' => "",
        'customer_b_security_deposite' => "",
        'customer_b_date' => "",
      )
    );
    $this->output
    ->set_content_type('application/json')
    ->set_status_header(HTTP_201)
    ->set_output(json_encode($response_array));
  }

}











}



?>
