<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'libraries/API_Controller.php');

class ProfileController extends API_Controller{


	public function __construct()
	{
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();
   $this->load->model('ProfileModel');


   $this->_APIConfig([
    'methods'                              => ['POST','GET'],
    'requireAuthorization'                 => true,
    'limit' => [100, 'ip', 'everyday'] ,
    'data' => [ 'status_code' => HTTP_401 ],
  ]);
 }


 public function getAllUserDetails(){
  header("Access-Control-Allow-Origin: *");

  $this->load->model('ProfileModel');
  $json_request_body = file_get_contents('php://input');
  $data = json_decode($json_request_body, true);

  if(isset($data['user_id'])){
    $user_id = $data['user_id'];

    if(empty($user_id)){
      $response_array = array(
       'status_code' => HTTP_201,
       'message' => USER_ID_MISSING,
     );
      $this->output
      ->set_content_type('application/json')
      ->set_status_header(HTTP_201)
      ->set_output(json_encode($response_array));
    }else{
      $user_array = array('user_id' => $user_id);
      $result_query = $this->ProfileModel->getUserDetails($user_id);
      if($result_query)
      {
        $response_array = array(
          'status_code' => HTTP_200,
          'message' => "User Details Received Successfully",
          'user_details' => array('user_id' => $result_query[0]['user_id'],
            'user_name' => $result_query[0]['user_username'],
            'user_mailid' => $result_query[0]['user_emailid'],
            'user_mobile_number' => $result_query[0]['user_mobilenumber'],
            'user_address' => $result_query[0]['user_address'],
            'user_profile_img' => $result_query[0]['user_profile_img'],
            'user_banner_img' => $result_query[0]['user_banner_img'],
            'user_office_number' => $result_query[0]['user_office_number'],
            'user_ower_name' => $result_query[0]['user_ower_name'],
            'user_agency_name' => $result_query[0]['user_agency_name'],
            'user_gst_number' => $result_query[0]['user_gst_number'],
          ),
          'nominee_details' => array('nominee_id' => $result_query[0]['nominee_id'],
            'nominee_name' => $result_query[0]['nominee_name'],
            'nominee_phone_number' => $result_query[0]['nominee_phone_number'],
            'nominee_address' => $result_query[0]['nominee_address'],
            'nominee_relation_nominee' => $result_query[0]['nominee_relation_nominee'],
            'nominee_created' => $result_query[0]['nominee_created']
          ),
          'bank_details' => array('bank_id' => $result_query[0]['bank_id'],
            'bank_account_number' => $result_query[0]['bank_account_number'],
            'bank_ifsc_code' => $result_query[0]['bank_ifsc_code'],
            'bank_act_holder_name' => $result_query[0]['bank_act_holder_name'],
            'pan_image' => $result_query[0]['pan_image'],
            'address_proof_image' => $result_query[0]['address_proof_image'],
            'pan_image_status' => $result_query[0]['pan_image_status'],
            'address_proof_status' => $result_query[0]['address_proof_status'],
            'bank_created' => $result_query[0]['bank_created'],
          ),

        );
        $this->output
        ->set_content_type('application/json')
        ->set_status_header(HTTP_200)
        ->set_output(json_encode($response_array));


        //$this->api_return(data, status_code);
      }
      else{
        $response_array = array(
          'status_code' => HTTP_201,
          'message' => SOMETHING_WRONG_RECEIVING_DATA
        );
        $this->output
        ->set_content_type('application/json')
        ->set_status_header(HTTP_201)
        ->set_output(json_encode($response_array));
      }

    }
  }
  else{
    $response_array = array(
      'status_code' => HTTP_201,
      'message' => NEED_ALL_PARAMS
    );
    $this->output
    ->set_content_type('application/json')
    ->set_status_header(HTTP_201)
    ->set_output(json_encode($response_array));
  }

}


public function updateAllUserProfileDetails(){
 $this->load->model('ProfileModel');
 $json_request_body = file_get_contents('php://input');
 $data = json_decode($json_request_body, true);


 if(isset($data['user_id']) &&
  isset($data['user_mobile_number']) && 
  isset($data['user_address']) 
  && isset($data['user_office_number'])
  && isset($data['user_ower_name'])
  && isset($data['user_agency_name'])
  && isset($data['user_gst_number']) ){

   $user_id = $data['user_id'];
 //$user_name = $data['user_name'];
 //$user_mailid = $data['user_mailid'];
 $user_mobile_number = $data['user_mobile_number'];
 $user_address = $data['user_address'];
 //$user_profile_img = $data['user_profile_img'];
 $user_office_number = $data['user_office_number'];
 $user_ower_name = $data['user_ower_name'];
 $user_agency_name = $data['user_agency_name'];
 $user_gst_number = $data['user_gst_number'];

 if(empty($user_id)){
  $response_array = array(
   'status_code' => HTTP_201,
   'message' => USER_ID_MISSING,
 );
  $this->output
  ->set_content_type('application/json')
  ->set_status_header(HTTP_201)
  ->set_output(json_encode($response_array));
}else{
  $user_array = array('user_id' => $user_id);
  $result_query = $this->ProfileModel->getUserDetails($user_id);
  //$db_user_name = $result_query[0]['user_username'];
  //$db_user_mailid = $result_query[0]['user_emailid'];
  $db_user_mobile_number = $result_query[0]['user_mobilenumber'];
  $db_user_address = $result_query[0]['user_address'];
  //$db_user_profile_img = $result_query[0]['user_profile_img'];
  $db_user_office_number = $result_query[0]['user_office_number'];
  $db_user_ower_name = $result_query[0]['user_ower_name'];
  $db_user_agency_name = $result_query[0]['user_agency_name'];
  $db_user_gst_number = $result_query[0]['user_gst_number'];

 if(empty($user_mobile_number)){
    $user_mobile_number=$db_user_mobile_number;
  } if(empty($user_address)){
    $user_address=$db_user_address;
  }if(empty($user_office_number)){
    $user_office_number=$db_user_office_number;
  }if(empty($user_ower_name)){
    $user_ower_name=$db_user_ower_name;
  }if(empty($user_agency_name)){
    $user_agency_name=$db_user_agency_name;
  }if(empty($user_gst_number)){
    $user_gst_number=$db_user_gst_number;
  }

  //$image_url_path = "uploads/profile/".$user_mobile_number.".png";

  $user_data = array(
    'user_mobilenumber' => $user_mobile_number,
    'user_address' => $user_address,
    'user_office_number' => $user_office_number,
    'user_ower_name' => $user_ower_name,
    'user_agency_name' => $user_agency_name,
    'user_gst_number' => $user_gst_number,
  );
  $result_query = $this->ProfileModel->updateUserDatas($user_id,$user_data);
  if($result_query)
  {
    $response_array = array(
      'status_code' => HTTP_200,
      'message' => USER_DETAILS_UPDATED,
    );
    $this->output
    ->set_content_type('application/json')
    ->set_status_header(HTTP_200)
    ->set_output(json_encode($response_array));
  }
  else{
    $response_array = array(
      'status_code' => HTTP_201,
      'message' => SOMETHING_WRONG_UPDATE_DATA
    );
    $this->output
    ->set_content_type('application/json')
    ->set_status_header(HTTP_201)
    ->set_output(json_encode($response_array));
  }
}
}
else{
  $response_array = array(
    'status_code' => HTTP_201,
    'message' => NEED_ALL_PARAMS
  );
  $this->output
  ->set_content_type('application/json')
  ->set_status_header(HTTP_201)
  ->set_output(json_encode($response_array));
}

}



public function updateProfileNomineeDetails(){
 $this->load->model('ProfileModel');
 $json_request_body = file_get_contents('php://input');
 $data = json_decode($json_request_body, true);


 if(isset($data['user_id']) && isset($data['nominee_name']) 
  && isset($data['nominee_phone_number']) && 
  isset($data['nominee_address']) && 
  isset($data['nominee_relation_nominee'])){

   $user_id = $data['user_id'];
 $nominee_name = $data['nominee_name'];
 $nominee_phone_number = $data['nominee_phone_number'];
 $nominee_address = $data['nominee_address'];
 $nominee_relation_nominee = $data['nominee_relation_nominee'];


 if(empty($user_id)){
  $response_array = array(
   'status_code' => HTTP_201,
   'message' => USER_ID_MISSING,
 );
  $this->output
  ->set_content_type('application/json')
  ->set_status_header(HTTP_201)
  ->set_output(json_encode($response_array));
}else{
  $user_array = array('user_id' => $user_id);
  $result_query = $this->ProfileModel->getNomineeDetails($user_id);
  $db_nominee_name = $result_query[0]['nominee_name'];
  $db_nominee_phone_number = $result_query[0]['nominee_phone_number'];
  $db_nominee_address = $result_query[0]['nominee_address'];
  $db_nominee_relation_nominee = $result_query[0]['nominee_relation_nominee'];


  if(empty($nominee_name)){
    $nominee_name = $db_nominee_name;
  } if(empty($nominee_phone_number)){
    $nominee_phone_number=$db_nominee_phone_number;
  } if(empty($nominee_address)){
    $nominee_address=$db_nominee_address;
  } if(empty($nominee_relation_nominee)){
    $nominee_relation_nominee=$db_nominee_relation_nominee;
  }

  //$image_url_path = "uploads/profile/".$user_mobile_number.".png";

  $user_data = array(
    'nominee_name' => $nominee_name,
    'nominee_phone_number' => $nominee_phone_number,
    'nominee_address' => $nominee_address,
    'nominee_relation_nominee' => $nominee_relation_nominee,
  );
  $result_query = $this->ProfileModel->updateNomineeDatas($user_id,$user_data);
  if($result_query)
  {
    $response_array = array(
      'status_code' => HTTP_200,
      'message' => NOMINEE_DETAILS_UPDATED,
    );
    $this->output
    ->set_content_type('application/json')
    ->set_status_header(HTTP_200)
    ->set_output(json_encode($response_array));
  }
  else{
    $response_array = array(
      'status_code' => HTTP_201,
      'message' => SOMETHING_WRONG_UPDATE_DATA
    );
    $this->output
    ->set_content_type('application/json')
    ->set_status_header(HTTP_201)
    ->set_output(json_encode($response_array));
  }
}
}
else{
  $response_array = array(
    'status_code' => HTTP_201,
    'message' => NEED_ALL_PARAMS
  );
  $this->output
  ->set_content_type('application/json')
  ->set_status_header(HTTP_201)
  ->set_output(json_encode($response_array));
}

}

public function updateProfileBankDetails(){
 $this->load->model('ProfileModel');
 $json_request_body = file_get_contents('php://input');
 $data = json_decode($json_request_body, true);


 if(isset($data['user_id']) && isset($data['bank_account_number']) 
  && isset($data['bank_ifsc_code']) && 
  isset($data['bank_act_holder_name'])){

   $user_id = $data['user_id'];
 $bank_account_number = $data['bank_account_number'];
 $bank_ifsc_code = $data['bank_ifsc_code'];
 $bank_act_holder_name = $data['bank_act_holder_name'];



 if(empty($user_id)){
  $response_array = array(
   'status_code' => HTTP_201,
   'message' => USER_ID_MISSING,
 );
  $this->output
  ->set_content_type('application/json')
  ->set_status_header(HTTP_201)
  ->set_output(json_encode($response_array));
}else{
  $user_array = array('user_id' => $user_id);
  $result_query = $this->ProfileModel->getBankDetails($user_id);
  $db_bank_account_number = $result_query[0]['bank_account_number'];
  $db_bank_ifsc_code = $result_query[0]['bank_ifsc_code'];
  $db_bank_act_holder_name = $result_query[0]['bank_act_holder_name'];



  if(empty($bank_account_number)){
    $bank_account_number = $db_bank_account_number;
  } if(empty($bank_ifsc_code)){
    $bank_ifsc_code=$db_bank_ifsc_code;
  } if(empty($bank_act_holder_name)){
    $bank_act_holder_name=$db_bank_act_holder_name;
  } 

  //$image_url_path = "uploads/profile/".$user_mobile_number.".png";

  $user_data = array(
    'bank_account_number' => $bank_account_number,
    'bank_ifsc_code' => $bank_ifsc_code,
    'bank_act_holder_name' => $bank_act_holder_name,
    
  );
  $result_query = $this->ProfileModel->updateBankDatas($user_id,$user_data);
  if($result_query)
  {
    $response_array = array(
      'status_code' => HTTP_200,
      'message' => BANK_DETAILS_UPDATED,
    );
    $this->output
    ->set_content_type('application/json')
    ->set_status_header(HTTP_200)
    ->set_output(json_encode($response_array));
  }
  else{
    $response_array = array(
      'status_code' => HTTP_201,
      'message' => SOMETHING_WRONG_UPDATE_DATA
    );
    $this->output
    ->set_content_type('application/json')
    ->set_status_header(HTTP_201)
    ->set_output(json_encode($response_array));
  }
}
}
else{
  $response_array = array(
    'status_code' => HTTP_201,
    'message' => NEED_ALL_PARAMS
  );
  $this->output
  ->set_content_type('application/json')
  ->set_status_header(HTTP_201)
  ->set_output(json_encode($response_array));
}

}



public function getNomineeRelationDetails(){
  $json_request_body = file_get_contents('php://input');
  $data = json_decode($json_request_body, true);


  $result_query = $this->ProfileModel->getNomineeRelation();
  $resultSet = Array();
  if($result_query)
  {
    foreach ($result_query as $product_result) 
    { 
      $resultSet[] = array(
        "nom_relation_id" =>  $product_result['nom_relation_id'],
        "nominee_relations" =>  $product_result['nominee_relations'],
        "nom_relation_created" =>  $product_result['nom_relation_created']
      );
    } 

    $response_array = array(
      'status' => HTTP_200,
      'message' => NOMINEE_RELATION_SUCCESS,
      'nominee_relations' => $resultSet
    );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }
  else{
    $response_array = array(
      'status' => HTTP_201,
      'message' => NOMINEE_RELATION_NOT_FOUND,
      'product_details' => $resultSet
    );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }
}





public function updateImageUploadForProfiles(){
 $this->load->model('ProfileModel');
 $json_request_body = file_get_contents('php://input');
 $data = json_decode($json_request_body, true);


 if(isset($data['user_id']) 
  && isset($data['user_profile_img']) 
  && isset($data['image_tag'])){

   $user_id = $data['user_id'];
 $image_tag = $data['image_tag'];
 $user_profile_img = $data['user_profile_img'];


 if(empty($user_id)){
  $response_array = array(
   'status_code' => HTTP_201,
   'message' => USER_ID_MISSING,
 );
  $this->output
  ->set_content_type('application/json')
  ->set_status_header(HTTP_201)
  ->set_output(json_encode($response_array));
} else if(empty($image_tag)){
  $response_array = array(
   'status_code' => HTTP_201,
   'message' => IMAGE_TAG_MISSING,
 );
  $this->output
  ->set_content_type('application/json')
  ->set_status_header(HTTP_201)
  ->set_output(json_encode($response_array));
}else if(empty($user_profile_img)){
  $response_array = array(
   'status_code' => HTTP_201,
   'message' => IMAGE_MISSING,
 );
  $this->output
  ->set_content_type('application/json')
  ->set_status_header(HTTP_201)
  ->set_output(json_encode($response_array));
}else if($image_tag == PROFILE_TAG){
  $user_array = array('user_id' => $user_id);
  $result_query = $this->ProfileModel->getUserDetails($user_id);

  $image_url_path = "uploads/profile/".$user_id."_profile.png";

  $user_data = array(
    'user_profile_img' => $image_url_path,
  );

  $result_query = $this->ProfileModel->updateUserDatas($user_id,$user_data);
  if($result_query)
  {
    if(!empty($user_profile_img)){
      $path = "uploads/profile/".$user_id."_profile.png";
      $user_profile_img = preg_replace('#data:image/[^;]+;base64,#', '', $user_profile_img);
      $status = file_put_contents($path,base64_decode($user_profile_img));
    }
    $response_array = array(
      'status_code' => HTTP_200,
      'message' => PROFILE_IMAGE_UPDATED,
    );
    $this->output
    ->set_content_type('application/json')
    ->set_status_header(HTTP_200)
    ->set_output(json_encode($response_array));
  }
  else{
    $response_array = array(
      'status_code' => HTTP_201,
      'message' => SOMETHING_WRONG_UPDATE_DATA
    );
    $this->output
    ->set_content_type('application/json')
    ->set_status_header(HTTP_201)
    ->set_output(json_encode($response_array));
  }
}else if($image_tag == PROFILE_TAG){
  $user_array = array('user_id' => $user_id);
  //$result_query = $this->ProfileModel->getUserDetails($user_id);

  $image_url_path = "uploads/profile/".$user_id."_profile.png";

  $user_data = array(
    'user_profile_img' => $image_url_path,
  );

  $result_query = $this->ProfileModel->updateUserDatas($user_id,$user_data);
  if($result_query)
  {
    if(!empty($user_profile_img)){
      $path = "uploads/profile/".$user_id."_profile.png";
      $user_profile_img = preg_replace('#data:image/[^;]+;base64,#', '', $user_profile_img);
      $status = file_put_contents($path,base64_decode($user_profile_img));
    }
    $response_array = array(
      'status_code' => HTTP_200,
      'message' => PROFILE_IMAGE_UPDATED,
    );
    $this->output
    ->set_content_type('application/json')
    ->set_status_header(HTTP_200)
    ->set_output(json_encode($response_array));
  }
  else{
    $response_array = array(
      'status_code' => HTTP_201,
      'message' => SOMETHING_WRONG_UPDATE_DATA
    );
    $this->output
    ->set_content_type('application/json')
    ->set_status_header(HTTP_201)
    ->set_output(json_encode($response_array));
  }
}else if($image_tag == BANNER_TAG){
  $user_array = array('user_id' => $user_id);
  //$result_query = $this->ProfileModel->getUserDetails($user_id);

  $image_url_path = "uploads/profile/".$user_id."_banner.png";

  $user_data = array(
    'user_banner_img' => $image_url_path,
  );

  $result_query = $this->ProfileModel->updateUserDatas($user_id,$user_data);
  if($result_query)
  {
    if(!empty($user_profile_img)){
      $path = "uploads/profile/".$user_id."_banner.png";
      $user_profile_img = preg_replace('#data:image/[^;]+;base64,#', '', $user_profile_img);
      $status = file_put_contents($path,base64_decode($user_profile_img));
    }
    $response_array = array(
      'status_code' => HTTP_200,
      'message' => BANNER_IMAGE_UPDATED,
    );
    $this->output
    ->set_content_type('application/json')
    ->set_status_header(HTTP_200)
    ->set_output(json_encode($response_array));
  }
  else{
    $response_array = array(
      'status_code' => HTTP_201,
      'message' => SOMETHING_WRONG_UPDATE_DATA
    );
    $this->output
    ->set_content_type('application/json')
    ->set_status_header(HTTP_201)
    ->set_output(json_encode($response_array));
  }
}else if($image_tag == PAN_TAG){
  $user_array = array('user_id' => $user_id);
  //$result_query = $this->ProfileModel->getUserDetails($user_id);

  $image_url_path = "uploads/bank/".$user_id."_pan.png";

  $user_data = array(
    'pan_image' => $image_url_path,
    'pan_image_status' => "Y"
  );

  $result_query = $this->ProfileModel->updateBankImageData($user_id,$user_data);
  if($result_query)
  {
    if(!empty($user_profile_img)){
      $path = "uploads/bank/".$user_id."_pan.png";
      $user_profile_img = preg_replace('#data:image/[^;]+;base64,#', '', $user_profile_img);
      $status = file_put_contents($path,base64_decode($user_profile_img));
    }
    $response_array = array(
      'status_code' => HTTP_200,
      'message' => PAN_IMAGE_UPDATED,
    );
    $this->output
    ->set_content_type('application/json')
    ->set_status_header(HTTP_200)
    ->set_output(json_encode($response_array));
  }
  else{
    $response_array = array(
      'status_code' => HTTP_201,
      'message' => SOMETHING_WRONG_UPDATE_DATA
    );
    $this->output
    ->set_content_type('application/json')
    ->set_status_header(HTTP_201)
    ->set_output(json_encode($response_array));
  }
}
else if($image_tag == ADDRESS_TAG){
  $user_array = array('user_id' => $user_id);
  //$result_query = $this->ProfileModel->getUserDetails($user_id);

  $image_url_path = "uploads/bank/".$user_id."_addressProof.png";

  $user_data = array(
    'address_proof_image' => $image_url_path,
    'address_proof_status' => "Y"
  );

  $result_query = $this->ProfileModel->updateBankImageData($user_id,$user_data);
  if($result_query)
  {
    if(!empty($user_profile_img)){
      $path = "uploads/bank/".$user_id."_addressProof.png";
      $user_profile_img = preg_replace('#data:image/[^;]+;base64,#', '', $user_profile_img);
      $status = file_put_contents($path,base64_decode($user_profile_img));
    }
    $response_array = array(
      'status_code' => HTTP_200,
      'message' => ADDRESS_IMAGE_UPDATED,
    );
    $this->output
    ->set_content_type('application/json')
    ->set_status_header(HTTP_200)
    ->set_output(json_encode($response_array));
  }
  else{
    $response_array = array(
      'status_code' => HTTP_201,
      'message' => SOMETHING_WRONG_UPDATE_DATA
    );
    $this->output
    ->set_content_type('application/json')
    ->set_status_header(HTTP_201)
    ->set_output(json_encode($response_array));
  }
}
else{
  $response_array = array(
    'status_code' => HTTP_201,
    'message' => NEED_ALL_PARAMS
  );
  $this->output
  ->set_content_type('application/json')
  ->set_status_header(HTTP_201)
  ->set_output(json_encode($response_array));
}

}


}}



?>
