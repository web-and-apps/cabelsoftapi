<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'libraries/API_Controller.php');

class CustomerEController extends API_Controller{


  public function __construct()
  {
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    parent::__construct();


    $this->_APIConfig([
      'methods'                              => ['POST','GET'],
      'requireAuthorization'                 => true,
      'limit' => [100, 'ip', 'everyday'] ,
      'data' => [ 'status_code' => HTTP_401 ],
    ]);

    $this->load->model('CustomerModel');
  }



  public function profileECustomerDatas(){

   $json_request_body = file_get_contents('php://input');
   $data = json_decode($json_request_body, true);

   if(isset($data['user_id']) &&
    isset($data['customer_id']) &&
    isset($data['customer_e_connection_date']) && 
    isset($data['customer_e_register_date'])){


     $user_id = $data['user_id'];
   $customer_id = $data['customer_id'];
   $customer_e_connection_date = $data['customer_e_connection_date'];
   $customer_e_register_date = $data['customer_e_register_date'];
   $customer_e_id = $data['customer_e_id'];


   if(empty($user_id)){
    $response_array = array(
     'status_code' => HTTP_400,
     'operation' => "",
     'message' => USER_ID_MISSING,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }   
  else if(empty($customer_id)){
    $response_array = array(
     'status_code' => HTTP_400,
     'operation' => "",
     'message' => CUSTOMER_ID_MISSING,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }
  else if(empty($customer_e_connection_date)){
    $response_array = array(
     'status_code' => HTTP_400,
     'operation' => "",
     'message' => NEED_CUSTOMER_CONNECTION_DATE,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }
  else if(empty($customer_e_register_date)){
    $response_array = array(
     'status_code' => HTTP_400,
     'operation' => "",
     'message' => NEED_CUSTOMER_REGISTER_DATE,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }


  else{

    if(empty($data['customer_e_id'])){

      $customer_array = array(
        'user_id' => $user_id,
        'customer_id' => $customer_id,
        'customer_e_connection_date' => $customer_e_connection_date,
        'customer_e_register_date' => $customer_e_register_date,
      );

      $result_query = $this->CustomerModel->addCustomerEModel($customer_array);
      if($result_query)
      {

        $customer_status_array = array(
          'customer_e_status' => CUSTOMER_STATUS_YES,
        );
        $this->CustomerModel->updateCustomerStatusDatas($customer_id,$customer_status_array);

        $response_array = array(
         'status_code' => HTTP_200,
         'operation' => OPERATION_ADDED,
         'message' => CUSTOMER_E_ENTRY_ADDED
       );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }
      else{
        $response_array = array(
         'status_code' => HTTP_400,
         'operation' => "",
         'message' => SOMETHING_WRONG_ADD_DATA,
       );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }

    }else{
      $customer_array = array(
        'customer_e_connection_date' => $customer_e_connection_date,
        'customer_e_register_date' => $customer_e_register_date
      );


      $result_query = $this->CustomerModel->updateCustomerEDatas($customer_e_id,$customer_array);
      if($result_query)
      {
        $response_array = array(
         'status_code' => HTTP_200,
         'operation' => OPERATION_UPDATED,
         'message' => CUSTOMER_E_ENTRY_UPDATE
       );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }
      else{
        $response_array = array(
         'status_code' => HTTP_400,
         'operation' => "",
         'message' => SOMETHING_WRONG_UPDATE_DATA,
       );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }
    }

  }
}
else{
  $response_array = array(
    'status_code' => HTTP_400,
    'operation' => "",
    'message' => NEED_ALL_PARAMS,
  );
  $this->output
  ->set_content_type('application/json')
  ->set_output(json_encode($response_array));
}
}


public function getprofileECustomerDetails(){
  header("Access-Control-Allow-Origin: *");

  $json_request_body = file_get_contents('php://input');
  $data = json_decode($json_request_body, true);

  if(isset($data['customer_id'])){
    $customer_id = $data['customer_id'];

    if(empty($customer_id)){
      $response_array = array(
       'status_code' => HTTP_201,
       'message' => CUSTOMER_ID_MISSING,
       'profile_e_details' => array(
        'customer_e_id' => "",
        'user_id' => "",
        'customer_id' => "",
        'customer_e_connection_date' => "",
        'customer_e_register_date' => "",
        'customer_e_date' => "",
      )
     );
      $this->output
      ->set_content_type('application/json')
      ->set_status_header(HTTP_201)
      ->set_output(json_encode($response_array));
    }else{
      $result_query = $this->CustomerModel->getCustomerEDetails($customer_id);
      if($result_query)
      {
        $response_array = array(
          'status_code' => HTTP_200,
          'message' => PROFILE_E_DETAILS_RECEIVED,
          'profile_e_details' => array(
            'customer_e_id' => $result_query[0]['customer_e_id'],
            'user_id' => $result_query[0]['user_id'],
            'customer_id' => $result_query[0]['customer_id'],
            'customer_e_connection_date' => $result_query[0]['customer_e_connection_date'],
            'customer_e_register_date' => $result_query[0]['customer_e_register_date'],
            'customer_e_date' => $result_query[0]['customer_e_date'],
          )
        );
        $this->output
        ->set_content_type('application/json')
        ->set_status_header(HTTP_200)
        ->set_output(json_encode($response_array));
      }
      else{
        $response_array = array(
          'status_code' => HTTP_201,
          'message' => SOMETHING_WRONG_RECEIVING_DATA,
          'profile_e_details' => array(
           'customer_e_id' => "",
           'user_id' => "",
           'customer_id' => "",
           'customer_e_connection_date' => "",
           'customer_e_register_date' => "",
           'customer_e_date' => "",
         )
        );
        $this->output
        ->set_content_type('application/json')
        ->set_status_header(HTTP_201)
        ->set_output(json_encode($response_array));
      }

    }
  }
  else{
    $response_array = array(
      'status_code' => HTTP_201,
      'message' => NEED_ALL_PARAMS,
      'profile_e_details' => array(
       'customer_e_id' => "",
       'user_id' => "",
       'customer_id' => "",
       'customer_e_connection_date' => "",
       'customer_e_register_date' => "",
       'customer_e_date' => "",
     )
    );
    $this->output
    ->set_content_type('application/json')
    ->set_status_header(HTTP_201)
    ->set_output(json_encode($response_array));
  }

}











}



?>
