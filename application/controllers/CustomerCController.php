<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'libraries/API_Controller.php');

class CustomerCController extends API_Controller{


	public function __construct()
	{
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();


    $this->_APIConfig([
      'methods'                              => ['POST','GET'],
      'requireAuthorization'                 => true,
      'limit' => [100, 'ip', 'everyday'] ,
      'data' => [ 'status_code' => HTTP_401 ],
    ]);

    $this->load->model('CustomerModel');
  }



  public function profileCCustomerDatas(){

   $json_request_body = file_get_contents('php://input');
   $data = json_decode($json_request_body, true);

   if(isset($data['user_id']) &&
    isset($data['customer_id']) &&
    isset($data['customer_c_bill_type']) && 
    isset($data['customer_c_bill_time']) && 
    isset($data['customer_c_additional_charge'])){


     $user_id = $data['user_id'];
   $customer_id = $data['customer_id'];
   $customer_c_bill_type = $data['customer_c_bill_type'];
   $customer_c_bill_time = $data['customer_c_bill_time'];
   $customer_c_additional_charge = $data['customer_c_additional_charge'];
   $customer_c_id = $data['customer_c_id'];


   if(empty($user_id)){
    $response_array = array(
     'status_code' => HTTP_400,
     'operation' => "",
     'message' => USER_ID_MISSING,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }   
  else if(empty($customer_id)){
    $response_array = array(
     'status_code' => HTTP_400,
     'operation' => "",
     'message' => CUSTOMER_ID_MISSING,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }
  else if(empty($customer_c_bill_type)){
    $response_array = array(
     'status_code' => HTTP_400,
     'operation' => "",
     'message' => NEED_CUSTOMER_BILLING_TYPE,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }
  else if(empty($customer_c_bill_time)){
    $response_array = array(
     'status_code' => HTTP_400,
     'operation' => "",
     'message' => NEED_CUSTOMER_BILLING_TIME,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }
  else if(empty($customer_c_additional_charge)){
    $response_array = array(
     'status_code' => HTTP_400,
     'operation' => "",
     'message' => NEED_CUSTOMER_ADDITIONAL_CHARGE,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }

  else{

    if(empty($data['customer_c_id'])){

      $customer_array = array(
       'user_id' => $user_id,
       'customer_id' => $customer_id,
       'customer_c_bill_type' => $customer_c_bill_type,
       'customer_c_bill_time' => $customer_c_bill_time,
       'customer_c_additional_charge' => $customer_c_additional_charge
     );

      $result_query = $this->CustomerModel->addCustomerCModel($customer_array);
      if($result_query)
      {

        $customer_status_array = array(
          'customer_c_status' => CUSTOMER_STATUS_YES,
          'customer_d_status' => CUSTOMER_STATUS_PROGRESS,
        );
        $this->CustomerModel->updateCustomerStatusDatas($customer_id,$customer_status_array);

        $response_array = array(
         'status_code' => HTTP_200,
         'operation' => OPERATION_ADDED,
         'message' => CUSTOMER_C_ENTRY_ADDED
       );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }
      else{
        $response_array = array(
         'status_code' => HTTP_400,
         'operation' => "",
         'message' => SOMETHING_WRONG_ADD_DATA,
       );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }

    }else{
      $customer_array = array(
        'customer_c_bill_type' => $customer_c_bill_type,
        'customer_c_bill_time' => $customer_c_bill_time,
        'customer_c_additional_charge' => $customer_c_additional_charge
      );


      $result_query = $this->CustomerModel->updateCustomerCDatas($customer_c_id,$customer_array);
      if($result_query)
      {
        $response_array = array(
         'status_code' => HTTP_200,
         'operation' => OPERATION_UPDATED,
         'message' => CUSTOMER_C_ENTRY_UPDATE
       );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }
      else{
        $response_array = array(
         'status_code' => HTTP_400,
         'operation' => "",
         'message' => SOMETHING_WRONG_UPDATE_DATA,
       );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }
    }

  }
}
else{
  $response_array = array(
    'status_code' => HTTP_400,
    'operation' => "",
    'message' => NEED_ALL_PARAMS,
  );
  $this->output
  ->set_content_type('application/json')
  ->set_output(json_encode($response_array));
}
}


public function getprofileCCustomerDetails(){
  header("Access-Control-Allow-Origin: *");

  $json_request_body = file_get_contents('php://input');
  $data = json_decode($json_request_body, true);

  if(isset($data['customer_id'])){
    $customer_id = $data['customer_id'];

    if(empty($customer_id)){
      $response_array = array(
       'status_code' => HTTP_201,
       'message' => CUSTOMER_ID_MISSING,
       'profile_c_details' => array('customer_c_id' => "",
        'user_id' => "",
        'customer_id' => "",
        'customer_c_bill_type' => "",
        'customer_c_bill_time' => "",
        'customer_c_additional_charge' => "",
        'customer_c_date' => "",
      )
     );
      $this->output
      ->set_content_type('application/json')
      ->set_status_header(HTTP_201)
      ->set_output(json_encode($response_array));
    }else{
      $result_query = $this->CustomerModel->getProfileCDetails($customer_id);
      if($result_query)
      {
        $response_array = array(
          'status_code' => HTTP_200,
          'message' => PROFILE_C_DETAILS_RECEIVED,
          'profile_c_details' => array('customer_c_id' => $result_query[0]['customer_c_id'],
           'user_id' => $result_query[0]['user_id'],
           'customer_id' => $result_query[0]['customer_id'],
           'customer_c_bill_type' => $result_query[0]['customer_c_bill_type'],
           'customer_c_bill_time' => $result_query[0]['customer_c_bill_time'],
           'customer_c_additional_charge' => $result_query[0]['customer_c_additional_charge'],
           'customer_c_date' => $result_query[0]['customer_c_date'],
         )
        );
        $this->output
        ->set_content_type('application/json')
        ->set_status_header(HTTP_200)
        ->set_output(json_encode($response_array));
      }
      else{
        $response_array = array(
          'status_code' => HTTP_201,
          'message' => SOMETHING_WRONG_RECEIVING_DATA,
          'profile_c_details' => array('customer_c_id' => "",
            'user_id' => "",
            'customer_id' => "",
            'customer_c_bill_type' => "",
            'customer_c_bill_time' => "",
            'customer_c_additional_charge' => "",
            'customer_c_date' => "",
          )
        );
        $this->output
        ->set_content_type('application/json')
        ->set_status_header(HTTP_201)
        ->set_output(json_encode($response_array));
      }

    }
  }
  else{
    $response_array = array(
      'status_code' => HTTP_201,
      'message' => NEED_ALL_PARAMS,
      'profile_c_details' => array('customer_c_id' => "",
        'user_id' => "",
        'customer_id' => "",
        'customer_c_bill_type' => "",
        'customer_c_bill_time' => "",
        'customer_c_additional_charge' => "",
        'customer_c_date' => "",
      )
    );
    $this->output
    ->set_content_type('application/json')
    ->set_status_header(HTTP_201)
    ->set_output(json_encode($response_array));
  }

}











}



?>
