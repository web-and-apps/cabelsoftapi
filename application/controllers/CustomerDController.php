<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'libraries/API_Controller.php');

class CustomerDController extends API_Controller{


	public function __construct()
	{
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();


    $this->_APIConfig([
      'methods'                              => ['POST','GET'],
      'requireAuthorization'                 => true,
      'limit' => [100, 'ip', 'everyday'] ,
      'data' => [ 'status_code' => HTTP_401 ],
    ]);

    $this->load->model('CustomerModel');
  }



  public function profileDCustomerDatas(){

   $json_request_body = file_get_contents('php://input');
   $data = json_decode($json_request_body, true);

   if(isset($data['user_id']) &&
    isset($data['customer_id']) &&
    isset($data['customer_d_settop_no']) && 
    isset($data['customer_d_settop_name']) && 
    isset($data['customer_d_settop_type']) && 
    isset($data['customer_d_smart_card_no'])){



     $user_id = $data['user_id'];
   $customer_id = $data['customer_id'];
   $customer_d_settop_no = $data['customer_d_settop_no'];
   $customer_d_settop_name = $data['customer_d_settop_name'];
   $customer_d_settop_type = $data['customer_d_settop_type'];
   $customer_d_smart_card_no = $data['customer_d_smart_card_no'];
   $customer_d_id = $data['customer_d_id'];



   if(empty($user_id)){
    $response_array = array(
     'status_code' => HTTP_400,
     'operation' => "",
     'message' => USER_ID_MISSING,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }   
  else if(empty($customer_id)){
    $response_array = array(
     'status_code' => HTTP_400,
     'operation' => "",
     'message' => CUSTOMER_ID_MISSING,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }
  else if(empty($customer_d_settop_no)){
    $response_array = array(
     'status_code' => HTTP_400,
     'operation' => "",
     'message' => NEED_CUSTOMER_SETTOP_NO,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }
  else if(empty($customer_d_settop_name)){
    $response_array = array(
     'status_code' => HTTP_400,
     'operation' => "",
     'message' => NEED_CUSTOMER_SETTOP_NAME,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }
  else if(empty($customer_d_settop_type)){
    $response_array = array(
     'status_code' => HTTP_400,
     'operation' => "",
     'message' => NEED_CUSTOMER_SETTOP_TYPE,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }
  else if(empty($customer_d_smart_card_no)){
    $response_array = array(
     'status_code' => HTTP_400,
     'operation' => "",
     'message' => NEED_CUSTOMER_SMART_NO,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }

  else{

    if(empty($data['customer_d_id'])){

      $customer_array = array(
        'user_id' => $user_id,
        'customer_id' => $customer_id,
        'customer_d_settop_no' => $customer_d_settop_no,
        'customer_d_settop_name' => $customer_d_settop_name,
        'customer_d_settop_type' => $customer_d_settop_type,
        'customer_d_smart_card_no' => $customer_d_smart_card_no
      );

      $result_query = $this->CustomerModel->addCustomerDModel($customer_array);
      if($result_query)
      {
        $customer_status_array = array(
          'customer_d_status' => CUSTOMER_STATUS_YES,
          'customer_e_status' => CUSTOMER_STATUS_PROGRESS,
        );
        $this->CustomerModel->updateCustomerStatusDatas($customer_id,$customer_status_array);

        $response_array = array(
         'status_code' => HTTP_200,
         'operation' => OPERATION_ADDED,
         'message' => CUSTOMER_D_ENTRY_ADDED
       );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }
      else{
        $response_array = array(
         'status_code' => HTTP_400,
         'operation' => "",
         'message' => SOMETHING_WRONG_ADD_DATA,
       );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }

    }else{
      $customer_array = array(
       'customer_d_settop_no' => $customer_d_settop_no,
       'customer_d_settop_name' => $customer_d_settop_name,
       'customer_d_settop_type' => $customer_d_settop_type,
       'customer_d_smart_card_no' => $customer_d_smart_card_no
     );


      $result_query = $this->CustomerModel->updateCustomerDDatas($customer_d_id,$customer_array);
      if($result_query)
      {
        $response_array = array(
         'status_code' => HTTP_200,
         'operation' => OPERATION_UPDATED,
         'message' => CUSTOMER_D_ENTRY_UPDATE
       );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }
      else{
        $response_array = array(
         'status_code' => HTTP_400,
         'operation' => "",
         'message' => SOMETHING_WRONG_UPDATE_DATA,
       );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }
    }

  }
}
else{
  $response_array = array(
    'status_code' => HTTP_400,
    'operation' => "",
    'message' => NEED_ALL_PARAMS,
  );
  $this->output
  ->set_content_type('application/json')
  ->set_output(json_encode($response_array));
}
}


public function getprofileDCustomerDetails(){
  header("Access-Control-Allow-Origin: *");

  $json_request_body = file_get_contents('php://input');
  $data = json_decode($json_request_body, true);

  if(isset($data['customer_id'])){
    $customer_id = $data['customer_id'];

    if(empty($customer_id)){
      $response_array = array(
       'status_code' => HTTP_201,
       'message' => CUSTOMER_ID_MISSING
     );
      $this->output
      ->set_content_type('application/json')
      ->set_status_header(HTTP_201)
      ->set_output(json_encode($response_array));
    }else{
      $result_query = $this->CustomerModel->getAllSettopBoxDatas($customer_id);
      $resultSet = Array();
      if($result_query)
      {
       foreach ($result_query as $customer_result) 
       { 
        $resultSet[] = array(
          "customer_d_id" =>  $customer_result['customer_d_id'],
          'user_id' =>$customer_result['user_id'],
          'customer_id' => $customer_result['customer_id'],
          "customer_d_settop_no" =>  $customer_result['customer_d_settop_no'],
          "customer_d_settop_name" =>  $customer_result['customer_d_settop_name'],
          "customer_d_settop_type" =>  $customer_result['customer_d_settop_type'],
          "customer_d_smart_card_no" =>  $customer_result['customer_d_smart_card_no'],
          "customer_d_settop_date" =>  $customer_result['customer_d_settop_date']
        );
      } 

      $response_array = array(
        'status_code' => HTTP_200,
        'message' => PROFILE_D_DETAILS_RECEIVED,
        'product_d_details' => $resultSet
      );

      $this->output
      ->set_content_type('application/json')
      ->set_status_header(HTTP_200)
      ->set_output(json_encode($response_array));
    }
    else{
      $response_array = array(
        'status_code' => HTTP_201,
        'message' => EMPTY_SETTOP_BOX,
        'profile_d_details' => $resultSet
      );
      $this->output
      ->set_content_type('application/json')
      ->set_status_header(HTTP_201)
      ->set_output(json_encode($response_array));
    }

  }
}
else{
  $response_array = array(
    'status_code' => HTTP_201,
    'message' => NEED_ALL_PARAMS,
    'profile_d_details' => $resultSet
  );
  $this->output
  ->set_content_type('application/json')
  ->set_status_header(HTTP_201)
  ->set_output(json_encode($response_array));
}

}











}



?>
