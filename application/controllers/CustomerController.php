<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'libraries/API_Controller.php');

class CustomerController extends API_Controller{


	public function __construct()
	{
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();


    $this->_APIConfig([
      'methods'                              => ['POST','GET'],
      'requireAuthorization'                 => true,
      'limit' => [100, 'ip', 'everyday'] ,
      'data' => [ 'status_code' => HTTP_401 ],
    ]);

    $this->load->model('CustomerModel');
  }



  public function profileACustomerDatas(){

   $json_request_body = file_get_contents('php://input');
   $data = json_decode($json_request_body, true);

   if(isset($data['user_id']) && 
    isset($data['customer_name']) && 
    isset($data['customer_area']) && 
    isset($data['customer_street']) &&
    isset($data['customer_home_type']) &&
    isset($data['customer_mobile'])){


     $user_id = $data['user_id'];
   $customer_name = $data['customer_name'];
   $customer_area = $data['customer_area'];
   $customer_street = $data['customer_street'];
   $customer_home_type = $data['customer_home_type'];
   $customer_mobile = $data['customer_mobile'];    
   $customer_id = $data['customer_id'];

   if(empty($user_id)){
    $response_array = array(
     'status_code' => HTTP_400,
     'customer_id' => "",
     'operation' => "",
     'message' => USER_ID_MISSING,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }
  else if(empty($customer_name)){
    $response_array = array(
     'status_code' => HTTP_400,
     'customer_id' => "",
     'operation' => "",
     'message' => NEED_CUSTOMER_NAME,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }
  else if(empty($customer_area)){
    $response_array = array(
     'status_code' => HTTP_400,
     'customer_id' => "",
     'operation' => "",
     'message' => NEED_CUSTOMER_AREA,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }
  else if(empty($customer_street)){
    $response_array = array(
     'status_code' => HTTP_400,
     'customer_id' => "",
     'operation' => "",
     'message' => NEED_CUSTOMER_STREET,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }
  else if(empty($customer_home_type)){
    $response_array = array(
     'status_code' => HTTP_400,
     'customer_id' => "",
     'operation' => "",
     'message' => NEED_CUSTOMER_HOME_TYPE,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }
  else if(empty($customer_mobile)){
    $response_array = array(
     'status_code' => HTTP_400,
     'customer_id' => "",
     'operation' => "",
     'message' => NEED_CUSTOMER_MOBILE,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }
  else{

    if(empty($data['customer_id'])){

      $customer_array = array(
       'user_id' => $user_id,
       'customer_name' => $customer_name,
       'customer_area' => $customer_area,
       'customer_street' => $customer_street,
       'customer_home_type' => $customer_home_type,
       'customer_mobile' => $customer_mobile
     );

      $result_query = $this->CustomerModel->addCustomerModel($customer_array);
      if($result_query)
      {
        $customer_status_array = array(
         'user_id' => $user_id,
         'customer_id'=>$result_query,
         'customer_a_status' => CUSTOMER_STATUS_YES,
         'customer_b_status' => CUSTOMER_STATUS_PROGRESS,
         'customer_c_status' => CUSTOMER_STATUS_NO,
         'customer_d_status' => CUSTOMER_STATUS_NO,
         'customer_e_status' => CUSTOMER_STATUS_NO
       );
        $this->CustomerModel->addCustomerStatusModel($customer_status_array);
        $response_array = array(
         'status_code' => HTTP_200,
         'customer_id' => $result_query,
         'operation' => OPERATION_ADDED,
         'message' => CUSTOMER_ENTRY_ADDED
       );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }
      else{
        $response_array = array(
         'status_code' => HTTP_400,
         'customer_id' => "",
         'operation' => "",
         'message' => SOMETHING_WRONG_ADD_DATA,
       );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }

    }else{
      $customer_array = array(
        'customer_name' => $customer_name,
        'customer_area' => $customer_area,
        'customer_street' => $customer_street,
        'customer_home_type' => $customer_home_type,
        'customer_mobile' => $customer_mobile
      );


      $result_query = $this->CustomerModel->updateCustomerDatas($customer_id,$customer_array);
      if($result_query)
      {
        $response_array = array(
         'status_code' => HTTP_200,
         'customer_id' => "",
         'operation' => OPERATION_UPDATED,
         'message' => CUSTOMER_ENTRY_UPDATED
       );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }
      else{
        $response_array = array(
         'status_code' => HTTP_400,
         'customer_id' => "",
         'operation' => "",
         'message' => SOMETHING_WRONG_UPDATE_DATA,
       );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }
    }

  }
}
else{
  $response_array = array(
    'status_code' => HTTP_400,
    'customer_id' => "",
    'operation' => "",
    'message' => NEED_ALL_PARAMS,
  );
  $this->output
  ->set_content_type('application/json')
  ->set_output(json_encode($response_array));
}
}


public function getprofileACustomerDetails(){
  header("Access-Control-Allow-Origin: *");

  $this->load->model('ProfileModel');
  $json_request_body = file_get_contents('php://input');
  $data = json_decode($json_request_body, true);

  if(isset($data['customer_id'])){
    $customer_id = $data['customer_id'];

    if(empty($customer_id)){
      $response_array = array(
       'status_code' => HTTP_201,
       'message' => CUSTOMER_ID_MISSING,
       'profile_a_details' => array('customer_id' => "",
        'user_id' => "",
        'customer_name' => "",
        'customer_area' => "",
        'customer_street' => "",
        'customer_home_type' => "",
        'customer_mobile' => "",
        'customer_date' => "",
      )
     );
      $this->output
      ->set_content_type('application/json')
      ->set_status_header(HTTP_201)
      ->set_output(json_encode($response_array));
    }else{
      $result_query = $this->CustomerModel->getProfileADetails($customer_id);
      if($result_query)
      {
        $response_array = array(
          'status_code' => HTTP_200,
          'message' => PROFILE_A_DETAILS_RECEIVED,
          'profile_a_details' => array('customer_id' => $result_query[0]['customer_id'],
            'user_id' => $result_query[0]['user_id'],
            'customer_name' => $result_query[0]['customer_name'],
            'customer_area' => $result_query[0]['customer_area'],
            'customer_street' => $result_query[0]['customer_street'],
            'customer_home_type' => $result_query[0]['customer_home_type'],
            'customer_mobile' => $result_query[0]['customer_mobile'],
            'customer_date' => $result_query[0]['customer_date'],
          )
        );
        $this->output
        ->set_content_type('application/json')
        ->set_status_header(HTTP_200)
        ->set_output(json_encode($response_array));
      }
      else{
        $response_array = array(
          'status_code' => HTTP_201,
          'message' => SOMETHING_WRONG_RECEIVING_DATA,
          'profile_a_details' => array('customer_id' => "",
            'user_id' => "",
            'customer_name' => "",
            'customer_area' => "",
            'customer_street' => "",
            'customer_home_type' => "",
            'customer_mobile' => "",
            'customer_date' => "",
          )
        );
        $this->output
        ->set_content_type('application/json')
        ->set_status_header(HTTP_201)
        ->set_output(json_encode($response_array));
      }

    }
  }
  else{
    $response_array = array(
      'status_code' => HTTP_201,
      'message' => NEED_ALL_PARAMS,
      'profile_a_details' => array('customer_id' => "",
        'user_id' => "",
        'customer_name' => "",
        'customer_area' => "",
        'customer_street' => "",
        'customer_home_type' => "",
        'customer_mobile' => "",
        'customer_date' => "",
      )
    );
    $this->output
    ->set_content_type('application/json')
    ->set_status_header(HTTP_201)
    ->set_output(json_encode($response_array));
  }

}




public function deleteCustomer(){
  $json_request_body = file_get_contents('php://input');
  $data = json_decode($json_request_body, true);

  if(isset($data['customer_id'])){
    $customer_id=$data['customer_id'];

    if(empty($customer_id)){
      $response_array = array(
        'status_code' => HTTP_400,
        'message' => CUSTOMER_ID_MISSING,
      );
      $this->output
      ->set_content_type('application/json')
      ->set_output(json_encode($response_array));
    }
    else{
      $result_query = $this->CustomerModel->deleteCustomerModel($customer_id);
      if($result_query)
      {
        $response_array = array(
          'status_code' => HTTP_200,
          'message' => CUSTOMER_ENTRY_DELETED
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }
      else{
        $response_array = array(
          'status_code' => HTTP_400,
          'message' => SOMETHING_WRONG_DELETE_DATA
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response_array));
      }
    }
  }
  else{
    $response_array = array(
      'status_code' => HTTP_400,
      'message' => NEED_ALL_PARAMS
    );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }
}



public function getAllCustomerDetails(){
  $json_request_body = file_get_contents('php://input');
  $data = json_decode($json_request_body, true);
  $resultSet = Array();

  if(isset($data['user_id']) 
    && isset($data['from_date']) 
    && isset($data['to_date'])
    && isset($data['page_count']) 
    && isset($data['search_keyword'])){
    $from_date = $data['from_date'];  
  $to_date = $data['to_date'];
  $page_count = $data['page_count'];
  $search_keyword = $data['search_keyword'];
  $user_id = $data['user_id'];

  if($page_count==''){
    $response_array = array(
      'status_code' => HTTP_400,
      'message' => PAGE_COUNT_MISSING,
      'customer_details' => $resultSet
    );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }else if(empty($user_id)){
    $response_array = array(
      'status_code' => HTTP_400,
      'message' => USER_ID_MISSING,
      'customer_details' => $resultSet
    );
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($response_array));
  }else{
    $page_count = ($page_count * 10);
    if(empty($from_date) && empty($to_date)){
      $result_query = $this->CustomerModel->getCustomerDetailsPage($user_id,$page_count,$search_keyword);
    }else{
      $result_query = $this->CustomerModel->getCustomerDetailsDate($user_id,$from_date,$to_date,$page_count,$search_keyword);
    }


    if($result_query)
    {
      foreach ($result_query as $customer_result) 
      { 
        $resultSet[] = array(
          "user_id" =>  $customer_result['user_id'],
          "customer_id" =>  $customer_result['customer_id'],
          "customer_name" =>  $customer_result['customer_name'],
          "customer_mobile" =>  $customer_result['customer_mobile'],
          "customer_date" =>  $customer_result['customer_date']
        );
      } 

      $response_array = array(
        'status_code' => HTTP_200,
        'message' => CUSTOMER_LIST_FOUND,
        'customer_details' => $resultSet
      );
      $this->output
      ->set_content_type('application/json')
      ->set_output(json_encode($response_array));
    }
    else{
      $response_array = array(
        'status_code' => HTTP_201,
        'message' => CUSTOMER_LIST_EMPTY,
        'customer_details' => $resultSet
      );
      $this->output
      ->set_content_type('application/json')
      ->set_output(json_encode($response_array));
    }
  }

}
else{
  $response_array = array(
    'status_code' => HTTP_400,
    'message' => NEED_ALL_PARAMS,
    'customer_details' => $resultSet
  );
  $this->output
  ->set_content_type('application/json')
  ->set_output(json_encode($response_array));
}

}



public function getCustomerTabStatusDetails(){
  header("Access-Control-Allow-Origin: *");

  $json_request_body = file_get_contents('php://input');
  $data = json_decode($json_request_body, true);

  if(isset($data['customer_id'])){
    $customer_id = $data['customer_id'];

    if(empty($customer_id)){
      $response_array = array(
       'status_code' => HTTP_201,
       'message' => CUSTOMER_ID_MISSING,
       'customer_profile_status' => array(
        'customer_status_id' => "",
        'user_id' => "",
        'customer_id' => "",
        'customer_a_status' => "",
        'customer_b_status' => "",
        'customer_c_status' => "",
        'customer_d_status' => "",
        'customer_e_status' => "",
        'customer_status_date' => "",
      )
     );
      $this->output
      ->set_content_type('application/json')
      ->set_status_header(HTTP_201)
      ->set_output(json_encode($response_array));
    }else{
      $result_query = $this->CustomerModel->getCustomerStatusDetails($customer_id);
      if($result_query)
      {
        $response_array = array(
          'status_code' => HTTP_200,
          'message' => CUSTOMER_PROFILE_STATUS,
          'customer_profile_status' => array(
            'customer_status_id' => $result_query[0]['customer_status_id'],
            'user_id' => $result_query[0]['user_id'],
            'customer_id' => $result_query[0]['customer_id'],
            'customer_a_status' => $result_query[0]['customer_a_status'],
            'customer_b_status' => $result_query[0]['customer_b_status'],
            'customer_c_status' => $result_query[0]['customer_c_status'],
            'customer_d_status' => $result_query[0]['customer_d_status'],
            'customer_e_status' => $result_query[0]['customer_e_status'],
            'customer_status_date' => $result_query[0]['customer_status_date'],
          )
        );
        $this->output
        ->set_content_type('application/json')
        ->set_status_header(HTTP_200)
        ->set_output(json_encode($response_array));
      }
      else{
        $response_array = array(
          'status_code' => HTTP_201,
          'message' => SOMETHING_WRONG_RECEIVING_DATA,
          'customer_profile_status' => array(
            'customer_status_id' => "",
            'user_id' => "",
            'customer_id' => "",
            'customer_a_status' => "",
            'customer_b_status' => "",
            'customer_c_status' => "",
            'customer_d_status' => "",
            'customer_e_status' => "",
            'customer_status_date' => "",
          )
        );
        $this->output
        ->set_content_type('application/json')
        ->set_status_header(HTTP_201)
        ->set_output(json_encode($response_array));
      }

    }
  }
  else{
    $response_array = array(
      'status_code' => HTTP_201,
      'message' => NEED_ALL_PARAMS,
      'customer_profile_status' => array(
        'customer_status_id' => "",
        'user_id' => "",
        'customer_id' => "",
        'customer_a_status' => "",
        'customer_b_status' => "",
        'customer_c_status' => "",
        'customer_d_status' => "",
        'customer_e_status' => "",
        'customer_status_date' => "",
      )
    );
    $this->output
    ->set_content_type('application/json')
    ->set_status_header(HTTP_201)
    ->set_output(json_encode($response_array));
  }

}


}



?>
