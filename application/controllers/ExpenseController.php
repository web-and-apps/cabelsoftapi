<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'libraries/API_Controller.php');

class ExpenseController extends API_Controller{


	public function __construct()
	{
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();


    $this->_APIConfig([
      'methods'                              => ['POST','GET'],
      'requireAuthorization'                 => true,
      'limit' => [100, 'ip', 'everyday'] ,
      'data' => [ 'status_code' => HTTP_401 ],
    ]);

    $this->load->model('ExpenseModel');
  }



  public function addNewExpenseDatas(){

   $json_request_body = file_get_contents('php://input');
   $data = json_decode($json_request_body, true);

   if(isset($data['expense_amount']) && isset($data['expense_comment']) && isset($data['user_id']) && isset($data['expense_type']) && isset($data['expense_user_date'])){

     $expense_amount = $data['expense_amount'];
     $expense_comment = $data['expense_comment'];    
     $expense_type = $data['expense_type'];   
     $expense_user_date = $data['expense_user_date'];    
     $user_id = $data['user_id']; 

     if(empty($user_id)){
      $response_array = array(
       'status_code' => HTTP_201,
       'message' => NEED_EXPENSE_COMMENTS,
     );
      $this->output
      ->set_content_type('application/json')
      ->set_status_header(HTTP_201)
      ->set_output(json_encode($response_array));
    }

    else if(empty($expense_amount)){
      $response_array = array(
       'status_code' => HTTP_201,
       'message' => NEED_EXPENSE_AMOUNT,
     );
      $this->output
      ->set_content_type('application/json')
      ->set_status_header(HTTP_201)
      ->set_output(json_encode($response_array));
    }
    else if(empty($expense_type)){
      $response_array = array(
       'status_code' => HTTP_201,
       'message' => NEED_EXPENSE_TYPE,
     );
      $this->output
      ->set_content_type('application/json')
      ->set_status_header(HTTP_201)
      ->set_output(json_encode($response_array));
    }
    else if(empty($expense_user_date)){
      $response_array = array(
       'status_code' => HTTP_201,
       'message' => NEED_EXPENSE_COMMENTS,
     );
      $this->output
      ->set_content_type('application/json')
      ->set_status_header(HTTP_201)
      ->set_output(json_encode($response_array));
    }
    else{

      $expense_array = array(
        'expense_amount' => $expense_amount,
        'expense_comment' => $expense_comment,
        'expense_type' => $expense_type,
        'expense_user_date' => $expense_user_date,
        'user_id' => $user_id
      );

      $result_query = $this->ExpenseModel->addExpenseModel($expense_array);
      if($result_query)
      {
        $response_array = array(
         'status_code' => HTTP_200,
         'message' => EXPENSE_ENTRY_ADDED
       );
        $this->output
        ->set_content_type('application/json')
        ->set_status_header(HTTP_200)
        ->set_output(json_encode($response_array));
      }
      else{
        $response_array = array(
         'status_code' => HTTP_201,
         'message' => SOMETHING_WRONG_ADD_DATA,
       );
        $this->output
        ->set_content_type('application/json')
        ->set_status_header(HTTP_201)
        ->set_output(json_encode($response_array));
      }

    }
  }
  else{
    $response_array = array(
      'status_code' => HTTP_201,
      'message' => NEED_ALL_PARAMS,
    );
    $this->output
    ->set_content_type('application/json')
    ->set_status_header(HTTP_201)
    ->set_output(json_encode($response_array));
  }
}

public function updateExpense(){
 $json_request_body = file_get_contents('php://input');
 $data = json_decode($json_request_body, true);


 if(isset($data['expense_id']) && isset($data['expense_amount']) && isset($data['expense_comment']) &&  isset($data['expense_type']) && isset($data['expense_user_date'])){

   $expense_id = $data['expense_id'];
   $expense_amount = $data['expense_amount'];
   $expense_comment = $data['expense_comment'];
   $expense_type = $data['expense_type'];   
   $expense_user_date = $data['expense_user_date'];    

   if(empty($expense_id)){
    $response_array = array(
     'status_code' => HTTP_201,
     'message' => EXPENSE_ID_MISSING,
   );
    $this->output
    ->set_content_type('application/json')
    ->set_status_header(HTTP_201)
    ->set_output(json_encode($response_array));
  }else{
    $expense_array = array('expense_id' => $expense_id);
    $result_query = $this->ExpenseModel->getExpenseDetails($expense_array);
    $db_expense_amount = $result_query[0]['expense_amount'];
    $db_expense_comment = $result_query[0]['expense_comment'];
    $db_expense_type = $result_query[0]['expense_type'];
    $db_expense_user_date = $result_query[0]['expense_user_date'];

    if(empty($expense_amount)){
      $expense_amount = $db_expense_amount;
    } if(empty($expense_comment)){
      $expense_comment=$db_expense_comment;
    }if(empty($expense_type)){
      $expense_type=$db_expense_type;
    }if(empty($expense_user_date)){
      $expense_user_date=$db_expense_user_date;
    }


    $expense_data = array(
      'expense_amount' => $expense_amount,
      'expense_comment' => $expense_comment,
      'expense_type' => $expense_type,
      'expense_user_date' => $expense_user_date,
    );
    $result_query = $this->ExpenseModel->updateExpenseDatas($expense_id,$expense_data);
    if($result_query)
    {
      $response_array = array(
        'status_code' => HTTP_200,
        'message' => EXPENSE_ENTRY_UPDATED,
      );
      $this->output
      ->set_content_type('application/json')
      ->set_status_header(HTTP_200)
      ->set_output(json_encode($response_array));
    }
    else{
      $response_array = array(
        'status_code' =>HTTP_201,
        'message' => SOMETHING_WRONG_UPDATE_DATA,
      );
      $this->output
      ->set_content_type('application/json')
      ->set_status_header(HTTP_201)
      ->set_output(json_encode($response_array));
    }
  }
}else{
  $response_array = array(
    'status_code' => HTTP_201,
    'message' => NEED_ALL_PARAMS,
  );
  $this->output
  ->set_content_type('application/json')
  ->set_status_header(HTTP_201)
  ->set_output(json_encode($response_array));
}

}



public function deleteExpense(){
  $json_request_body = file_get_contents('php://input');
  $data = json_decode($json_request_body, true);

  if(isset($data['expense_id'])){
    $expense_id=$data['expense_id'];

    if(empty($expense_id)){
      $response_array = array(
        'status_code' => HTTP_201,
        'message' => EXPENSE_ID_MISSING,
      );
      $this->output
      ->set_content_type('application/json')
      ->set_status_header(HTTP_201)
      ->set_output(json_encode($response_array));
    }
    else{
      $result_query = $this->ExpenseModel->deleteExpenseModel($expense_id);
      if($result_query)
      {
        $response_array = array(
          'status_code' => HTTP_200,
          'message' => EXPENSE_ENTRY_DELETED
        );
        $this->output
        ->set_content_type('application/json')
        ->set_status_header(HTTP_200)
        ->set_output(json_encode($response_array));
      }
      else{
        $response_array = array(
          'status_code' => HTTP_201,
          'message' => SOMETHING_WRONG_DELETE_DATA
        );
        $this->output
        ->set_content_type('application/json')
        ->set_status_header(HTTP_201)
        ->set_output(json_encode($response_array));
      }
    }
  }
  else{
    $response_array = array(
      'status_code' => HTTP_201,
      'message' => NEED_ALL_PARAMS
    );
    $this->output
    ->set_content_type('application/json')
    ->set_status_header(HTTP_201)
    ->set_output(json_encode($response_array));
  }
}



public function getAllExpenseDetails(){
  $json_request_body = file_get_contents('php://input');
  $data = json_decode($json_request_body, true);

  if(isset($data['user_id']) && isset($data['from_date']) && isset($data['to_date']) && isset($data['page_count']) && isset($data['search_keyword'])){
    $user_id = $data['user_id'];
    $from_date = $data['from_date'];
    $to_date = $data['to_date'];
    $page_count = $data['page_count'];
    $search_keyword = $data['search_keyword'];

    if(empty($user_id)){
      $response_array = array(
        'status_code' => HTTP_201,
        'message' => USER_ID_MISSING,
      );
      $this->output
      ->set_content_type('application/json')
      ->set_status_header(HTTP_201)
      ->set_output(json_encode($response_array));
    }
    else if($page_count==''){
      $response_array = array(
        'status_code' => HTTP_201,
        'message' => PAGE_COUNT_MISSING,
      );
      $this->output
      ->set_content_type('application/json')
      ->set_status_header(HTTP_201)
      ->set_output(json_encode($response_array));
    }else{
      $page_count = ($page_count * 10);
      if(empty($from_date) && empty($to_date)){
        $result_query = $this->ExpenseModel->getExpenseDetailsPage($user_id,$page_count,$search_keyword);
      }else{
        $result_query = $this->ExpenseModel->getExpenseDetailsDate($user_id,$from_date,$to_date,$page_count,$search_keyword);
      }

      $resultSet = Array();
      if($result_query)
      {
        foreach ($result_query as $expense_result) 
        { 
          $resultSet[] = array(
            "expense_id" =>  $expense_result['expense_id'],
            "expense_amount" =>  $expense_result['expense_amount'],
            "expense_comment" =>  $expense_result['expense_comment'],
            "expense_date" =>  $expense_result['expense_date'],
            "expense_type" =>  $expense_result['expense_type'],
            "expense_user_date" =>  $expense_result['expense_user_date']
          );
        } 

        $response_array = array(
          'status_code' => HTTP_200,
          'message' => EXPENSE_LIST_FOUND,
          'product_details' => $resultSet
        );
        $this->output
        ->set_content_type('application/json')
        ->set_status_header(HTTP_200)
        ->set_output(json_encode($response_array));
      }
      else{
        $response_array = array(
          'status_code' => HTTP_201,
          'message' => EXPENSE_LIST_EMPTY,
          'product_details' => $resultSet
        );
        $this->output
        ->set_content_type('application/json')
        ->set_status_header(HTTP_201)
        ->set_output(json_encode($response_array));
      }
    }



  }
  else{
    $response_array = array(
      'status_code' => HTTP_201,
      'message' => NEED_ALL_PARAMS
    );
    $this->output
    ->set_content_type('application/json')
    ->set_status_header(HTTP_201)
    ->set_output(json_encode($response_array));
  }

}



}



?>
